FROM node:alpine3.17

WORKDIR /App 

COPY ./package.json .

RUN npm install --omit=dev

COPY . ./

USER node

EXPOSE 3000

CMD ["npm", "start"]